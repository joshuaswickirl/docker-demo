FROM alpine:3.9.2

ADD /bin/helloWorld /

ENV PATH=$PATH:/

CMD ["helloWorld"]